#!/usr/bin/env ruby

# http://ruby-doc.org/stdlib-1.9.3/libdoc/net/imap/rdoc/Net/IMAP.html
# http://www.rubydoc.info/github/mikel/mail/Mail/Envelope
# http://www.rubydoc.info/github/mikel/mail/Mail/Header
# See also:  imap.rb https://gist.github.com/viking/3164382

require 'mail'
require 'net/imap'
require 'openssl'
# require 'will_paginate/array'
require 'pry'
require 'byebug'
require 'cgi'

cgi = CGI.new
print cgi.header
# byebug
print "Made it here!\n"

# encoding: utf-8

subjects = []

imap = Net::IMAP.new( 'localhost' )
# imap = Net::IMAP.new( 'localhost', 4500 )	# ssh -N -L 4510:localhost:25 -L 4500:localhost:143 phil@prix	# on another terminal
# imap = Net::IMAP.new( 'prix' )
# imap = Net::IMAP.new( 'pricom.com.au' )
# imap = Net::IMAP.new( 'pricom.com.au', 143, false )
# imap = Net::IMAP.new( 'pricom.com.au', 443 )
# imap = Net::IMAP.new( 'pricom.com.au', 993, true, nil, false )
# options = {:host => "pricom.com.au", :ssl => true, :outfile => "messages.csv"}
# imap = Net::IMAP.new(options[:host], :ssl => options[:ssl])

# binding.pry

# imap.put_StartTls(true)
# imap.put_Port(143)

# byebug

imap.login( 'username', 'passwd' )	# without ssh tunneling - gets to here and fails because of complaint about plain text . . 
imap.examine( 'INBOX' )
# imap.examine( '0_spam' )
# # smtp = Net::SMTP.new( srvr, 25 )
 
# print imap.search(["UNSEEN"])
# puts ''
# puts ''

while true
#	system( "clear" )
# imap.search(['ALL']).each do |message_id|
	imap.search([ "UNSEEN" ]).each do |message_id|
#		print message_id.to_s + ' '
#		printf "%3d ", message_id.to_s 
#		envelope = imap.fetch( message_id, "ENVELOPE" )[0].attr[ "ENVELOPE" ]
#		from = "#{envelope.from[0].mailbox}@#{envelope.from[0].host}"
#		subj = envelope.subject
#		subj = Mail::Encodings.value_decode( envelope.subject )
#		from = imap.fetch(message_id, "BODY[HEADER.FIELDS (FROM)]")
#		p from[0]
#		subj = Mail::Encodings.value_decode( imap.fetch(message_id, "BODY[HEADER.FIELDS (SUBJECT)]") )
#		subjects.push( subj )
#		p subj
#		body = imap.fetch(message_id,'BODY[TEXT]')[0].attr['BODY[TEXT]']

		msg = imap.fetch(message_id,'RFC822')[0].attr['RFC822']
		mail = Mail.read_from_string msg 
		subjects.push( Mail::Encodings.value_decode( [message_id.to_s, mail.subject] ))
	end

	print "<p>"

	subjects.reverse.each { |mail|
		printf "%3d ", mail[0].to_i
		print mail[1]
		print "<br>"
	}

	print "<\/p>"

imap.logout

exit

